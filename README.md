
**⚠ WARNING: This project has been moved to [gitlab.incoresemi.com](https://gitlab.incoresemi.com).
It will soon be archived and eventually deleted.**

#Quickstart Guide
This repository holds prebuilt binaries for Ubuntu 18.04
Run the following commands to download prebuilt binaries of 64-bit RISCV GNU Toolchain, spike (modified) and openocd

```
git clone https://gitlab.com/incoresemi/tools/riscv-tools-prebuilt.git
RISCVTOOLS=$PWD
export PATH=$PATH:$RISCVTOOLS/openocd:$RISCVTOOLS/riscv64-unknown-elf/bin:$RISCVTOOLS/riscv-isa-sim-mod/build/bin
```
